package com.demo.covid.count.controller;

import java.io.IOException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;

import com.demo.covid.count.model.CovidData;
import com.demo.covid.count.service.DataService;

@Controller
public class ViewController {
	
	@Autowired
	private DataService dataService;

	@GetMapping("/")
	public String display(ModelMap model) throws IOException, InterruptedException {
		
		dataService.fetchData();
		
		
		List<CovidData> topAll = dataService.topCountry(10);
		model.addAttribute("topAll", topAll);
		model.addAttribute("topAllLimit", 10);
		
		List<CovidData> rankSEA = dataService.rankingSEA();
		model.addAttribute("sea", rankSEA);
		
		List<CovidData> all = dataService.all();
		model.addAttribute("all", all);
		
		model.addAttribute("totalConfirmed", dataService.getTOTAL_CONFIRMED_COUNT());
		model.addAttribute("totalDeath", dataService.getTOTAL_DEATH_COUNT());
		model.addAttribute("totalRecovered", dataService.getTOTAL_RECOVERED_COUNT());

		return "index";
	}
	
}
