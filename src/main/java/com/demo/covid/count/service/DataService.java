package com.demo.covid.count.service;

import java.io.IOException;
import java.io.StringReader;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Optional;
import java.util.stream.Collectors;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVRecord;
import org.springframework.stereotype.Service;

import com.demo.covid.count.model.CovidData;
import com.demo.covid.count.model.CovidRowData;

@Service
public class DataService {

	private List<CovidData> COVID_CASES = new ArrayList<>();
	private int TOTAL_CONFIRMED_COUNT = 0;
	private int TOTAL_DEATH_COUNT = 0;
	private int TOTAL_RECOVERED_COUNT = 0;
	
	private String CONFIRMED_CASES_URL = "https://raw.githubusercontent.com/CSSEGISandData/COVID-19/master/csse_covid_19_data/"+
	          "csse_covid_19_time_series/time_series_covid19_confirmed_global.csv";
	private String DEATHS_URL = "https://raw.githubusercontent.com/CSSEGISandData/COVID-19/master/csse_covid_19_data/"
			+ "csse_covid_19_time_series/time_series_covid19_deaths_global.csv";
	private String RECOVERED_URL = "https://raw.githubusercontent.com/CSSEGISandData/COVID-19/master/csse_covid_19_data/"
			+ "csse_covid_19_time_series/time_series_covid19_recovered_global.csv";
	
	public void fetchData() throws IOException, InterruptedException{
		COVID_CASES.clear();
		TOTAL_CONFIRMED_COUNT = 0;
		TOTAL_DEATH_COUNT = 0;
		TOTAL_RECOVERED_COUNT = 0;
		
//		if(COVID_CASES.size() == 0) {
			
			// GET RAW DATA FROM CSV
			List<CovidRowData> rawConfirmedCases = getDataFromResource(CONFIRMED_CASES_URL);
			List<CovidRowData> rawDeathCases = getDataFromResource(DEATHS_URL);
			List<CovidRowData> rawRecoveredCases = getDataFromResource(RECOVERED_URL);
			
			countTotalCasesPerCountry(
					rawConfirmedCases, 
					rawDeathCases, 
					rawRecoveredCases);
//		}
	}

	public List<CovidData> topCountry(int limit) {
		
		List<CovidData> top = sortDataRevesed(COVID_CASES, limit);
		
		return top;
	}
	
	public List<CovidData> all() {
		
		return COVID_CASES;
	}
	
	public List<CovidData> rankingSEA() {

		List<String> seaCountries = new ArrayList<>();
		seaCountries.add("Indonesia");
		seaCountries.add("Vietnam");
		seaCountries.add("Thailand");
		seaCountries.add("Singapore");
		seaCountries.add("Malaysia");
		seaCountries.add("Philippines");
		seaCountries.add("Laos");
		seaCountries.add("Cambodia");
		seaCountries.add("Burma");
		seaCountries.add("Brunei");
		seaCountries.add("Timor-Leste");
		
		List<CovidData> result = new ArrayList<>();
		
		for(String country : seaCountries) {
			
			Optional<CovidData> data = COVID_CASES.stream()
					.filter(entry -> country.equals(entry.getCountry()))
					.findFirst();
			
			if(data.isPresent()) {
				CovidData cov = new CovidData(country, 
						data.get().getTotalConfirmed(),
						data.get().getTotalDeath(),
						data.get().getTotalRecovered());
				result.add(cov);
			}
		}
		return sortDataRevesed(result, 10);
	}
	
	

	public int getTOTAL_CONFIRMED_COUNT() {
		return TOTAL_CONFIRMED_COUNT;
	}

	public int getTOTAL_DEATH_COUNT() {
		return TOTAL_DEATH_COUNT;
	}

	public int getTOTAL_RECOVERED_COUNT() {
		return TOTAL_RECOVERED_COUNT;
	}

	private List<CovidData> sortDataRevesed(List<CovidData> list, int limit) {
		
		return list.stream() 
				.sorted((CovidData cd1, CovidData cd2) -> 
					Long.compare(cd2.getTotalConfirmed(), cd1.getTotalConfirmed()))
				.limit(limit)
				.collect(Collectors.toList());
	}
	
	private void countTotalCasesPerCountry(
			List<CovidRowData> rawConfirmedCases, 
			List<CovidRowData> rawDeathCases,
			List<CovidRowData> rawRecoveredCases) {

		// GET TOTAL PER COUNTRY PER DAY
		Map<String, Map<String, Integer>> totalConfirmedPerCountry = 
				getTotalCountPerCountry(rawConfirmedCases);
		
		Map<String, Map<String, Integer>> totalDeathsPerCountry = 
				getTotalCountPerCountry(rawDeathCases);
		
		Map<String, Map<String, Integer>> totalRecoveredPerCountry = 
				getTotalCountPerCountry(rawRecoveredCases);		
		
		
		for(Entry<String, Map<String, Integer>> set : totalConfirmedPerCountry.entrySet()) {
			
			String country = set.getKey();
			Map<String, Integer> confirmedCases = set.getValue();
			Map<String, Integer> deathCases = totalDeathsPerCountry.get(country);
			Map<String, Integer> recoveredCases = totalRecoveredPerCountry.get(country);
			
			Optional<CovidRowData> confirmedLatest = rawConfirmedCases
					.stream()
					.filter(data -> country.equals(data.getCountry()))
					.findFirst();
			Optional<CovidRowData> deathLatest = rawDeathCases
					.stream()
					.filter(data -> country.equals(data.getCountry()))
					.findFirst();
			Optional<CovidRowData> recoveredLatest = rawRecoveredCases
					.stream()
					.filter(data -> country.equals(data.getCountry()))
					.findFirst();		
			
			int confirmedCount = (confirmedLatest.isPresent() ? confirmedLatest.get().getLatestCount() : 0);
			int deathCount = (deathLatest.isPresent() ? deathLatest.get().getLatestCount() : 0);
			int recoveredCount = (recoveredLatest.isPresent() ? recoveredLatest.get().getLatestCount() : 0);
			TOTAL_CONFIRMED_COUNT += confirmedCount;
			TOTAL_DEATH_COUNT += deathCount;
			TOTAL_RECOVERED_COUNT += recoveredCount;
			
			COVID_CASES.add(new CovidData(country, 
					confirmedCount, deathCount, recoveredCount,
					confirmedCases, deathCases, recoveredCases));
		}
	}

	private Map<String, Map<String, Integer>> getTotalCountPerCountry(List<CovidRowData> rawData) {
		
		Map<String, Map<String, Integer>> totalPerCountry = new LinkedHashMap<>();
				
		List<String> dateLabel = rawData.get(0).getData();
        for(int i = 1; i < rawData.size(); i++) {
        	
        	String country = rawData.get(i).getCountry();
        	Map<String, Integer> holder = new LinkedHashMap<>();
        	if(totalPerCountry.containsKey(country)) 
        		 holder = totalPerCountry.get(country);

        	Map<String, Integer> newValue = new LinkedHashMap<>();
        	List<String> countData = rawData.get(i).getData();
        	
        	for(int j = 0; j <dateLabel.size(); j++ ) {
        		
        		String date = dateLabel.get(j);
        		int total = Integer.parseInt(countData.get(j)) + 
        				(holder.get(date) == null ? 0 : holder.get(date));
        		newValue.put(date, total);
        	}
        	
        	totalPerCountry.put(country, newValue);
        }
        
        return totalPerCountry;
	}

	private List<CovidRowData> getDataFromResource(String url) throws IOException, InterruptedException {
		
		HttpClient client = HttpClient.newHttpClient();
        HttpRequest request = HttpRequest.newBuilder()
                .uri(URI.create(url))
                .build();

        HttpResponse<String> response = client.send(request,
                HttpResponse.BodyHandlers.ofString());
        
        List<CovidRowData> covidData = new ArrayList<>();
        
        StringReader reader = new StringReader(response.body());
        Iterable<CSVRecord> records = CSVFormat.DEFAULT.parse(reader);

        for (CSVRecord record : records) {
        	String country = record.get(1);
        	List<String> list = new ArrayList<>();
        	
        	// index 4 to record.size - 1
        	for(int i = 4; i < record.size(); i++) {
        		list.add(record.get(i));
        	}
        	
        	int latestCount = 0;
        	try{
        		latestCount = Integer.parseInt(record.get(record.size() - 1));
        	}
        	catch(Exception e) {}
        	
			CovidRowData data = new CovidRowData(country, latestCount, list);
			covidData.add(data);
        }
        
        return covidData;
	}

}
