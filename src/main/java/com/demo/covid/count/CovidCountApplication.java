package com.demo.covid.count;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CovidCountApplication {

	public static void main(String[] args) {
		SpringApplication.run(CovidCountApplication.class, args);
	}

}
