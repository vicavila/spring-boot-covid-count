package com.demo.covid.count.model;

import java.util.List;

public class CovidRowData {

	private String country;
	private Integer latestCount;
	private List<String> data;

	public CovidRowData(String country, Integer latestCount, List<String> data) {
		super();
		this.country = country;
		this.latestCount = latestCount;
		this.data = data;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public Integer getLatestCount() {
		return latestCount;
	}

	public void setLatestCount(Integer latestCount) {
		this.latestCount = latestCount;
	}

	public List<String> getData() {
		return data;
	}

	public void setData(List<String> data) {
		this.data = data;
	}

	@Override
	public String toString() {
		return "CovidRowData [country=" + country + ", latestCount=" + latestCount + ", data=" + data + "]";
	}

}
