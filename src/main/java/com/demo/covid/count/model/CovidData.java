package com.demo.covid.count.model;

import java.util.Map;

public class CovidData {

	private String country;
	private Integer totalConfirmed;
	private Integer totalDeath;
	private Integer totalRecovered;
	private Map<String, Integer> confirmedMap;
	private Map<String, Integer> deathMap;
	private Map<String, Integer> recoveredMap;
	
	
	public CovidData(String country, Integer totalConfirmed, Integer totalDeath, Integer totalRecovered) {
		super();
		this.country = country;
		this.totalConfirmed = totalConfirmed;
		this.totalDeath = totalDeath;
		this.totalRecovered = totalRecovered;
	}

	public CovidData(String country, Integer totalConfirmed, Integer totalDeath, Integer totalRecovered,
			Map<String, Integer> confirmedMap, Map<String, Integer> deathMap, Map<String, Integer> recoveredMap) {
		super();
		this.country = country;
		this.totalConfirmed = totalConfirmed;
		this.totalDeath = totalDeath;
		this.totalRecovered = totalRecovered;
		this.confirmedMap = confirmedMap;
		this.deathMap = deathMap;
		this.recoveredMap = recoveredMap;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	
	
	public Integer getTotalConfirmed() {
		return totalConfirmed;
	}

	public void setTotalConfirmed(Integer totalConfirmed) {
		this.totalConfirmed = totalConfirmed;
	}

	
	public Integer getTotalDeath() {
		return totalDeath;
	}

	public void setTotalDeath(Integer totalDeath) {
		this.totalDeath = totalDeath;
	}

	public Integer getTotalRecovered() {
		return totalRecovered;
	}

	public void setTotalRecovered(Integer totalRecovered) {
		this.totalRecovered = totalRecovered;
	}

	
	
	public Map<String, Integer> getConfirmedMap() {
		return confirmedMap;
	}

	public void setConfirmedMap(Map<String, Integer> confirmedMap) {
		this.confirmedMap = confirmedMap;
	}

	public Map<String, Integer> getDeathMap() {
		return deathMap;
	}

	public void setDeathMap(Map<String, Integer> deathMap) {
		this.deathMap = deathMap;
	}

	public Map<String, Integer> getRecoveredMap() {
		return recoveredMap;
	}

	public void setRecoveredMap(Map<String, Integer> recoveredMap) {
		this.recoveredMap = recoveredMap;
	}

	@Override
	public String toString() {
		return "CovidData [country=" + country + ", totalConfirmed=" + totalConfirmed + ", totalDeath=" + totalDeath
				+ ", totalRecovered=" + totalRecovered + "]";
	}


}
